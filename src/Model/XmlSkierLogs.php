﻿<?php
/**
  * This file is a part of the code used in IMT2571 Assignment 5.
  *
  * @author Rune Hjelsvold
  * @version 2018
  */

require_once('Club.php');
require_once('Skier.php');
require_once('YearlyDistance.php');
require_once('Affiliation.php');

/**
  * The class for accessing skier logs stored in the XML file
  */
class XmlSkierLogs
{
    protected $doc;

    public function __construct($url) {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
    }


    public function getClubs() {
        $clubs = array();
		//Creating Xpath for using xpath queries in function,
		//getting errors when I initialize xpath in constructor instead
		$xpath = new DOMXpath($this->doc);
		//Querying for all clubs
		$clubQuery = $xpath->query('/SkierLogs/Clubs/Club');

	//Going through all clubs to get the parameters required for the club contructor
	//Using the DOM function getElementsByTagName to get the correct childnodes 
	foreach($clubQuery as $club) {
	    $name = $club->getElementsByTagName('Name')->item(0)->nodeValue;
	    $city = $club->getElementsByTagName('City')->item(0)->nodeValue;
	    $county = $club->getElementsByTagName('County')->item(0)->nodeValue;
	    $id = $club->getAttribute('id');		
	    //Storing into a temp and pushing into the clubs array
	    $tempC = new Club($id, $name, $city, $county);   
	    array_push($clubs, $tempC);
	}         
	return $clubs;
    }

    public function getSkiers() {
        
		$skiers = array();
 		//Creating Xpath for using xpath queries in function
		$xpath = new DOMXpath($this->doc);
		//Queries for all skiers	
		$skierQuery = $xpath->query('/SkierLogs/Skiers/Skier');
        
		//Going through all skier queries to find parameters used in skier constructor
	foreach($skierQuery as $skier) {
	    $firstName = $skier->getElementsByTagName('FirstName')->item(0)->nodeValue;
	    $lastName = $skier->getElementsByTagName('LastName')->item(0)->nodeValue;
	    $yoBirth = $skier->getElementsByTagName('YearOfBirth')->item(0)->nodeValue;
	    $userName = $skier->getAttribute('userName');			
	    //Storing results into a temp, but not pushing to array yet as there's more to add
	    $tempS = new Skier($userName, $firstName, $lastName, $yoBirth);	
	
	    //Querying for cases where affiliations should be added 
	    //(username of skier in season aligns with username of skier in skiers array)
	    //also has values of yearly distances covered	
	    $affdistQuery = $xpath->query("/SkierLogs/Season/Skiers/Skier[@userName='$userName']");
			
	    //Goes through all nodes where affiliations should be added
	    //and gets parameters necessary for affiliation constructor
	    foreach($affdistQuery as $affiliation) {
			//Makes sure clubId is read correctly with no null or invalid values
			if ($affiliation->parentNode->getAttribute('clubId')) {
		    	$clubId = $affiliation->parentNode->getAttribute('clubId');
	            $clubSeason = $affiliation->parentNode->parentNode->getAttribute('fallYear');
		    	//Storing results into a temp, not pushing until distances are added			
		    	$tempS->addAffiliation(new Affiliation($clubId, $clubSeason));
	        }
	    }	
	
	    //Goes through distances in seasons and affiliates it to a skier 				
	    foreach($affdistQuery as $distances) {
			$fall = $distances->parentNode->parentNode->getAttribute('fallYear');
			//Resets distance total for skier
			$distValue = 0;		
			//Goes through each distance and adds to the yearly total
		foreach($distances->getElementsByTagName('Distance') as $distance){
		    $distValue += $distance->nodeValue;
		}			
		//Adds the yearly total distances to array of skiers
		$tempS->addYearlyDistance(new YearlyDistance($fall, $distValue));
	    }		
	    //Pushes temp skier array to main skiers array
	    array_push($skiers, $tempS);
	}		
    return $skiers;
    }
}
?>
